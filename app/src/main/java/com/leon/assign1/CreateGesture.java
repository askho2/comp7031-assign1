package com.leon.assign1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.content.Intent;

import android.util.Log;

public class CreateGesture extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener {

    public GestureOverlayView gestureView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture);
        this.setTitle("Create Gesture");
        this.gestureView = findViewById(R.id.gestureView);
        this.gestureView.addOnGesturePerformedListener(this);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, final android.gesture.Gesture gesture) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("What is this gesture used for?");
        builder.setItems(new String[]{"Back", "Next"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(CreateGesture.this, MainActivity.class);
                intent.putExtra("gesture", gesture);
                intent.putExtra("mode", which);
                startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
