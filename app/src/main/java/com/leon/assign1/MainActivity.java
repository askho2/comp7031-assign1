package com.leon.assign1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GestureDetectorCompat;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener {

    private int[] images;
    private ImageView imageView;
    private int imageCount = 0;

    private Gesture backGesture;
    private Gesture forwardGesture;

    private GestureDetectorCompat detector;
    private GestureLibrary mlib;
    private ConstraintLayout layout;
    Animation slideOutRightAnim, slideInLeft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.images = new int[] {R.drawable.img1, R.drawable.img2, R.drawable.img3};
        this.imageView = findViewById(R.id.imageView);
        detector = new GestureDetectorCompat(this, new MyGestureListener());
        mlib = GestureLibraries.fromRawResource(this, R.raw.gestures);
        mlib.load();
        Intent intent = getIntent();
        int mode = intent.getIntExtra("mode", -1);
        if (mode >= 0) {
            Gesture gesture = intent.getExtras().getParcelable("gesture");
            this.addGesture(mode, gesture);
        }

        GestureOverlayView overlay = findViewById(R.id.gestureView);
        overlay.addOnGesturePerformedListener(this);
        layout = findViewById(R.id.parentLayout);
        slideOutRightAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        slideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        slideOutRightAnim.setAnimationListener(new SlideRightAnimationListener());
    }


    public void onPrevClick(View view) {
        imageCount--;
        if (imageCount < 0) {
            imageCount = this.images.length - 1;
        }
        ObjectAnimator prevAnimator = ObjectAnimator.ofFloat(this.imageView, "translationX", this.imageView.getWidth() * -1 );
        prevAnimator.setDuration(250);
        prevAnimator.start();
        prevAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                ImageView imageView = MainActivity.this.imageView;
                imageView.setImageResource(MainActivity.this.images[imageCount]);
                imageView.setX(MainActivity.this.layout.getWidth());
                ObjectAnimator prevAnimator = ObjectAnimator.ofFloat(imageView, "translationX",0);
                prevAnimator.setDuration(250);
                prevAnimator.start();
            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }
        });

    }

    private void addGesture(int mode, Gesture gesture) {
        if (mode == 0) {
            backGesture = gesture;
        }
        else {
            forwardGesture = gesture;
        }
        Set<String> gestures = mlib.getGestureEntries();
        Iterator<String> iterator = gestures.iterator();
        while(iterator.hasNext()) {
            String key = iterator.next();
            if (key != "back" || key != "forward") {
                iterator.remove();
                mlib.removeEntry(key);
            }
        }

        if (backGesture != null) {
            mlib.addGesture("back", backGesture);
        }

        if(forwardGesture != null) {
            mlib.addGesture("forward", forwardGesture);
        }
    }
    // this doesnt get called anymore because we put the gestureoverlay on.
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        this.detector.onTouchEvent(event);
//        return super.onTouchEvent(event);
//    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        this.detector.onTouchEvent(event);
        super.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    public void onNextClick(View view) {
        imageCount++;
        if (imageCount > this.images.length - 1) {
            this.imageCount = 0;
        }
        this.imageView.startAnimation(slideOutRightAnim);
    }

    public void onGestureClick(View view) {
        Intent intent = new Intent(this, CreateGesture.class);
        startActivity(intent);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mlib.recognize(gesture);

        // We want at least one prediction
        if (predictions.size() > 0) {
            Prediction prediction = predictions.get(0);
            // We want at least some confidence in the result
            if (prediction.score > 1) {
                // Show the spell
                if (prediction.name == "forward") {
                    this.onNextClick(this.imageView);
                }
                else if (prediction.name == "back") {
                    this.onPrevClick(this.imageView);
                }
            }
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if (velocityX < 0 && velocityY < 300 && velocityY > -300) {
                MainActivity.this.onPrevClick((findViewById(R.id.content)));
            }
            else if (velocityX > 0 && velocityY < 300 && velocityY > -300){
                MainActivity.this.onNextClick((findViewById(R.id.content)));
            }
            return true;
        }
    }

    private class SlideRightAnimationListener implements Animation.AnimationListener {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // DO nothing
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            // Sliding to right completed should slide in new photo from left.
            imageCount++;
            if (imageCount > MainActivity.this.images.length - 1) {
                MainActivity.this.imageCount = 0;
            }
            MainActivity.this.imageView.setImageResource((MainActivity.this.images[imageCount]));
            MainActivity.this.imageView.startAnimation(MainActivity.this.slideInLeft);
        }
    }
}
